class Rack::Attack
 
    Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new
    
    # Allow all localhost traffic
    safelist('allow-localhost') do |req|
      '127.0.0.1' == req.ip || '::1' == req.ip
    end
    
    # Implementing Rate Limit
    throttle('req/ip', limit: 5, period: 5) do |req|
      req.ip
    end
   end