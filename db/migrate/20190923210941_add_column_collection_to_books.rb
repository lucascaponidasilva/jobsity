class AddColumnCollectionToBooks < ActiveRecord::Migration[5.2]
  def up
    add_column :books, :collection, :uuid    
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end