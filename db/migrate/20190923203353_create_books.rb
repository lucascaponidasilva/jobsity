class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books, id: :uuid do |t|
      t.string :title
      t.string :author
      t.string :isbn 
      t.decimal :price, precision: 5, scale: 2
      t.text :short_description

      t.timestamps
    end
  end
end
