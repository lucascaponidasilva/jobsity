class RenameColumnCollectionUuidToCollectionIdInBooks < ActiveRecord::Migration[5.2]
  def up
    rename_column :books, :collection, :collection_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end