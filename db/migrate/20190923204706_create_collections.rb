class CreateCollections < ActiveRecord::Migration[5.2]
  def change
    create_table :collections, id: :uuid do |t|
      t.string :title
      t.references :book, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
