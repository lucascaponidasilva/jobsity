# README

Lucas Caponi da Silva

**Ruby version**  
'2.6.3'

**Rails version**  
'5.2.3'

**------------------**  
**How to run project**

Please run the following commands:

docker-compose build

docker-compose run web rake db:create

docker-compose run web rake db:migrate

docker-compose run web rake db:seed

docker-compose up -d

**------------------**  
**How to run the test suite**  
 docker-compose run web rspec

**------------------**  
**How to change RATE LIMIT?**  
 Go to the file config/initializers/rack_attack.rb  
 And change the throttle limit and period

**------------------**  
**API Endpoints:**  
 **To get all books:**  
 GET /api/v1/books

**To get a specific book by title:**  
 GET /api/v1/books?filter[title]=NAME_OF_THE_BOOK

**To get a specific book by isbn:**  
 GET /api/v1/books?filter[isbn]=ISBN_NUMBER

**To get a specific book by uuid:**  
 GET /api/v1/books?filter[id]=UUID_NUMBER

**To get the collection of a specific book:**  
 GET /api/v1/books/BOOK_UUID/collection

**To get a specific collection by title:**  
 GET /api/v1/collections?filter[title]=NAME_OF_THE_COLLECTION

**To get a specific collection by title:**  
 GET /api/v1/collections?filter[id]=UUID_NUMBER

**To get a specific collection by title:**  
 GET /api/v1/collections?filter[id]=UUID_NUMBER

**To get all books from a collection:**  
 GET /api/v1/collections/COLLECTION_UUID/books

**To get all collections and their books:**  
 GET /api/v1/collections?include=books

**To get a collection by title and their books:**  
 GET /api/v1/collections?include=books&filter[title]=Arron

**------------------**  
**Paginating results:**  
 **books:**  
 GET /books?page%5Bnumber%5D=10&page%5Bsize%5D=10  
 GET /collections?page%5Bnumber%5D=10&page%5Bsize%5D=10

**To create a book:**  
 POST /api/v1/books

**To create a collection:**  
 POST /api/v1/collections

**To delete a book:**  
 DELETE /api/v1/books/:id

**To delete a collection:**  
 DELETE /api/v1/collections/:id
