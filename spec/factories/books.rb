# frozen_string_literal: true

FactoryBot.define do
  factory :book do
    title { Faker::Name.first_name }
    author { Faker::Name.first_name }
    isbn { Faker::Code.isbn }
    price { 9.3 }
    short_description { Faker::Lorem.paragraph_by_chars(number: chars = 256, supplemental: supplemental = false) }
    collection { nil }
  end

  trait :with_collection do
    after :create do |this_book|
      new_collection = FactoryBot.create(:collection)
      new_collection.books.append(this_book)
      this_book.collection = new_collection
    end
  end
end
