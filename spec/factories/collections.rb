# frozen_string_literal: true

FactoryBot.define do
  factory :collection do
    title { Faker::Name.first_name }
  end

  trait :with_one_book do
    after :create do |this_collection|
      this_collection.append(FactoryBot.create(:book, collection: this_collection))
    end
  end

  trait :with_books do
    after :create do |this_collection|
      3.times do
        this_collection.books.append(FactoryBot.create(:book, collection: this_collection))
      end
    end
  end
end
