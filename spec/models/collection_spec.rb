# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Collection, type: :model do
  describe 'Associations' do
    it { should have_many(:books) }
  end
end
