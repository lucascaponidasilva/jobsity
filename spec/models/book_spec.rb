# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Book, type: :model do
  describe 'Associations' do
    it { should belong_to(:collection).optional }
  end

  describe 'When creating a book' do
    it 'should return a new item' do
      new_book = create(:book)
      expect(new_book).to be_valid
    end
  end
end
