# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::BooksController, type: :controller do
    describe 'When request for books' do
        context 'with valid data' do 
            it 'should return ok on index' do 
                response = get :index, format: :json
                expect(response).to have_http_status(:success)
            end

            it 'should return ok on show' do 
                response = get :show, params: { id: create(:book).id }, format: :json
                expect(response).to have_http_status(:success)
            end
        end
    end
end
