# frozen_string_literal: true

module Api::V1
  class BookResource < JSONAPI::Resource
    attributes :title, :isbn, :author, :price, :short_description
    filters :id, :title, :isbn
    has_one :collection
    exclude_links %i[self related]
  end
end
