# frozen_string_literal: true

module Api::V1
  class BaseApiController < JSONAPI::ResourceController
  end
end
