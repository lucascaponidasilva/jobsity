# frozen_string_literal: true

module Api::V1
  class CollectionResource < JSONAPI::Resource
    attributes :title
    filters :id, :title
    has_many :books
  end
end
