# frozen_string_literal: true

class Book < ApplicationRecord
  belongs_to :collection, optional: true
end
